#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"

./stop_server.sh

rm nohup.out

nohup target/release/jachymek-web-server &

echo "Server started."
