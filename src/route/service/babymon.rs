use actix_session::Session;
use actix_web::{get, HttpResponse, post, delete, put, Responder, web};
use serde::Deserialize;

#[get("/babymon/environment_data")]
pub(crate) async fn get_environment_data(data: web::Data<AppData>) -> impl Responder {

    Command::new("sh")
        .arg("-c")
        .arg("/home/kotelnik/bin/humidity.pyhello")
        .output()
        .expect("failed to execute process")

    let shout = Shout::find_newest(&data.get_ref().pool);
    match shout {
        Ok(res) => HttpResponse::Ok().json(res),
        Err(err) => match err {
            diesel::result::Error::DatabaseError(_, info) => HttpResponse::InternalServerError().body(info.message().to_string()),
            diesel::result::Error::NotFound => HttpResponse::NotFound().finish(),
            _ => HttpResponse::InternalServerError().body("other_error"),
        },
    }
}
